import React from "react";
import { Map, Marker} from 'yandex-map-react';
import axios from 'axios';


class MapBasics extends React.Component {
    state ={
        center: [42.811211, 73.856801],
        coords: [],
        width: "100%",
        height: 650,
        address: ''
    };

    onMapClick(event) {
        const coords = event.get('coords');
        this.setState({
            coords: coords,
        });


        axios.get(`https://geocode-maps.yandex.ru/1.x/?apikey=8e09f2ad-3118-4b05-86ed-8ac9c19b9c12&geocode=${this.state.coords[1]},${this.state.coords[0]}&format=json`)
            .then(response=>{
                const nameAddress = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.name;
                const areas = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.description;
                const fullAddress = nameAddress + ',' + areas;
                this.setState({address: fullAddress})
            });
    }

    render() {
        return(
            <div>
                <div className="confirm">
                    <h1>От {this.state.address}</h1>
                    <button>Подтвердить</button>
                </div>
            <Map
                onAPIAvailable={
                    function () {
                        console.log('API loaded');
                    }
                }
                center={this.state.center}
                width={this.state.width}
                height={this.state.height}
                zoom={10}
                onClick={this.onMapClick.bind(this)}
            >
                {
                    this.state.coords.map(coords=>(
                        <Marker
                            lat={this.state.coords[0]}
                            lon={this.state.coords[1]}
                            iconContent={this.state.address}
                        />
                    ))
                }
            </Map>
            </div>
        )
    }

}

export default MapBasics;