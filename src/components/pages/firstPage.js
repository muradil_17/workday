import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from './../../axios';
import './firstPage.css';
import './media.css';
import {faCarSide, faTruck, faWalking} from "@fortawesome/free-solid-svg-icons";
import {Map, Marker} from "yandex-map-react";
import Loader from "../preLoader/loader";

class FirstPage extends Component {

    state={
        address: '',
        preOrder: false,
        number: '',
        numberCourier: '',
        numberUser: '',
        numberFirst: '+996',
        transferAddress: '',
        nameUser: '',
        distance: 0,
        modework: [],
        tariff: '',
        date: '',
        point: 0,
        timeTransfer: '',
        disableTime: true,
        productTransferUser:[],
        product: '',
        otherProduct: '',
        bigSizeProduct: false,
        payUser:[],
        payment: '',
        cond: false,
        center: [42.811211, 73.856801],
        coords: [],
        coordsTransfer: [],
        width: "100%",
        height: 650,
        showMap: false,
        transferMapShow: false,
        firstIcon: '',
        spin: true
    };


    componentDidMount() {
        axios.get('tariffs').then(response =>{
            const tariff = [];
            for (let key in response.data.data){
                tariff.push({...response.data.data[key]});
            }
            this.setState({modework: tariff, spin: false})
        });

        axios.get('cargoTypes').then(response =>{
            const productTransferUser = [];
            for (let key in response.data.data){
                productTransferUser.push({...response.data.data[key], id:key});
            }
            this.setState({productTransferUser: productTransferUser})
        });

        axios.get('paymenttypes').then(response =>{
            const payUser = [];
            for (let key in response.data.data){
                payUser.push({...response.data.data[key], id: key});
            }
            this.setState({payUser: payUser})
        });

    }

    onClickTariffs= (event) =>{
        let value = event.currentTarget.value;
        if(this.state.modework === ''){
            return false
        }else{
            console.log(event.currentTarget.value);
            this.setState({
                tariff: value,
            })
        }
    };


    fromMapClick(event) {
        const coords = event.get('coords');
        this.setState({
            coords: coords,
        });


        axios.get(`https://geocode-maps.yandex.ru/1.x/?apikey=8e09f2ad-3118-4b05-86ed-8ac9c19b9c12&geocode=${this.state.coords[1]},${this.state.coords[0]}&format=json`)
            .then(response=>
            {
                const nameAddress = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.name;
                const areas = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.description;
                const fullAddress = nameAddress + ',' + areas;
                this.setState({address: fullAddress})
            });
    }

    toMapClick(e) {
        const coords = e.get('coords');
        this.setState({
            coordsTransfer: coords,
        });


        axios.get(`https://geocode-maps.yandex.ru/1.x/?apikey=8e09f2ad-3118-4b05-86ed-8ac9c19b9c12&geocode=${this.state.coordsTransfer[1]},${this.state.coordsTransfer[0]}&format=json`)
            .then(response=>
            {
                const nameAddress = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.name;
                const areas = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.description;
                const fullAddress = nameAddress + ',' + areas;
                this.setState({transferAddress: fullAddress})
            });
    }


    textValueChanged = e =>{
      this.setState({[e.target.name]: e.target.value})
    };

    checkHandle = e =>{
      this.setState({preOrder: e.target.checked})
    };

    sizeProductChanged = e =>{
      this.setState({bigSizeProduct: e.target.checked})
    };

    userCondHandle = e =>{
        this.setState({cond: e.target.checked})
    };

    radioCheckHandle = e =>{
        this.setState({timeTransfer: e.target.value});
        if (this.state.timeTransfer === 'Как можнее скорее'){
            this.setState({disableTime: false})
        }else if(this.state.timeTransfer === 'Выбрать время') {
            this.setState({disableTime: true})
        }
    };

    productTransferHandle = e =>{
      this.setState({product: e.target.value});
    };

    payUserHandle = e =>{
      this.setState({payment: e.target.value});
    };

    mapShowUser = e =>{
        this.setState({showMap: true})
    };

    mapCloseUser = e =>{
        if(this.state.address === ''){
            return false
        }
        this.setState({showMap: false})
    };

    transferMapShowUser = e =>{
        this.setState({transferMapShow: true})
    };

    transferMapCloseUser = e =>{
        if (this.state.transferAddress === ''){
            return false
        }
        this.setState({transferMapShow: false})
    };

    orderClickUser = e =>{
      const order = {
          customer: {
              name: this.state.nameUser,
              telNumber: this.state.numberFirst + this.state.numberUser,
          },
          addresses: [
              {
                  title: this.state.address,
                  coordinates: {
                      lat: this.state.coords[0],
                      long:  this.state.coords[1]
                  },
                  addressee: null
              },
              {
                  title: this.state.transferAddress,
                  coordinates: {
                      lat: this.state.coordsTransfer[0],
                      long: this.state.coordsTransfer[1]
                  },
                  addressee: {
                      name: this.state.nameUser,
                      telNumber: this.state.numberCourier
                  }
              }
          ],
          deliveryTime: this.state.date,
          tariff: this.state.tariff,
          cargoType: this.state.product,
          notes: this.state.otherProduct,
          isLargeCargo: this.state.bigSizeProduct,
          paymentType: this.state.payment,
          attachedCourier: null
      };


      if (this.state.address === '' && this.state.nameUser === '' &&
          this.state.tariff === '')
      {
          return false
      }

      axios.post('/orders', order).finally(()=>{
          console.log('complete');
      });
    };



    render() {
        let product = this.state.productTransferUser;

        let productTransferUser = product.map((product, i)=>{
           return(
               <div className="typeProduct" key={i}>
               <p>
                   <input
                       name='product'
                       type="radio"
                       value={product._id}
                       onChange={this.productTransferHandle}
                   />{product.title}
               </p>
               </div>
           )
        });

        let tariffs = this.state.modework;


        let tariffsClick = tariffs.map((tariffs, k) =>{
        return(
               <div key={k}>
                   <button
                       className="onFoot"
                       value={tariffs._id}
                       onClick={this.onClickTariffs}
                   >
                       {
                           tariffs.alt === 'medium' ? <FontAwesomeIcon icon={faCarSide} className='drive'/> : <FontAwesomeIcon icon={faWalking} className='walking'/>
                           && tariffs.alt === 'heavy' ? <FontAwesomeIcon icon={faTruck} className='driving'/> : <FontAwesomeIcon icon={faWalking} className='walking'/>
                       }
                       <div className="onFootText">
                           <h1>{tariffs.title}</h1>
                           <p>Допустимый вес: {tariffs.weight.from} - {tariffs.weight.to} кг</p>
                       </div>
                   </button>
               </div>
        )});


        let payUser = this.state.payUser;

        let payment = payUser.map((pay, j)=>{
           return(
                   <p key={j}>
                       <input
                           type="radio"
                           name='sistemPay'
                           value={pay._id}
                           onChange={this.payUserHandle}
                       />{pay.title}
                   </p>
           )
        });

        const mainOrder = ( <div className='firstPage'>
            <header className='header'>
                <div className="textheader">
                    <a href='/'>
                        germes delivery
                    </a>
                </div>
            </header>
            <div className="big-content">
                <div className="item-1">
                    <div className="pickUp">
                        <div className="address">
                            <h1>Откуда забрать</h1>
                            <div className="userAddress">
                                <h4>Адрес</h4>
                                <input
                                    type='button'
                                    name='address'
                                    className='addressText'
                                    onClick={this.mapShowUser}
                                    value={this.state.address}
                                />
                                <div className="allcheck">
                                    <input
                                        type='checkbox'
                                        id='point'
                                        checked={this.state.preOrder}
                                        onChange={this.checkHandle}
                                        className='checkPayment'
                                        name='preOrder'
                                    />
                                    <label htmlFor='point' className='checkPoint'>Курьеру нужно будет произвести оплату за груз?</label>
                                </div>
                            </div>
                            <div className="userNumber">
                                <h3>Номер отправителя (если есть)</h3>
                                <span className='numberText'>{this.state.numberFirst}
                                    <input
                                        type="tel"
                                        minLength="18"
                                        maxLength="18"
                                        placeholder='(___) ___ ___'
                                        name='numberCourier'
                                        onChange={this.textValueChanged}
                                        value={this.state.numberCourier}
                                    /></span>
                            </div>
                        </div>
                    </div>
                    <div className="transfer">
                        <div className="address">
                            <h1>Куда отвезти</h1>
                            <div className="userAddress">
                                <h4>Адрес</h4>
                                <input
                                    type='button'
                                    className='addressText'
                                    name='transferAddress'
                                    onClick={this.transferMapShowUser}
                                    value={this.state.transferAddress}
                                />
                            </div>
                            <div className="userNumber">
                                <h3>Номер телефона</h3>
                                <span className='numberText'>{this.state.numberFirst}
                                    <input
                                        type="tel"
                                        minLength="18"
                                        maxLength="18"
                                        placeholder='(___) ___ ___'
                                        name='numberUser'
                                        onChange={this.textValueChanged}
                                        value={this.state.numberUser}
                                    /></span>
                            </div>
                            <div className="userName">
                                <h3>Имя получателя</h3>
                                <input
                                    type="text"
                                    className='nameUser'
                                    name='nameUser'
                                    onChange={this.textValueChanged}
                                    value={this.state.nameUser}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="mode">
                        <div className="modeTransfer">
                            <h1>Срочность доставки</h1>
                            <p>
                                <input
                                    type="radio"
                                    name='timeTransfer'
                                    value='Как можнее скорее'
                                    onChange={this.radioCheckHandle}
                                />Как можно скорее
                            </p>
                            <p>
                                <input
                                    type="radio"
                                    name='timeTransfer'
                                    value='Выбрать время'
                                    onChange={this.radioCheckHandle}
                                />Выбрать время
                            </p>
                        </div>
                        <div className="dataTransfer">
                            <input
                                type="datetime-local"
                                name='date'
                                onChange={this.textValueChanged}
                                disabled={this.state.disableTime}
                            />
                        </div>
                    </div>
                </div>
                <div className="item-2">
                    <div className="modeWork">
                        <h1>Как доставить</h1>
                        <div className="workCourier">
                            { tariffsClick }
                        </div>
                    </div>
                    <div className="product">
                        <h1>Что доставить</h1>
                        {productTransferUser}
                    </div>
                    <div className="other">
                            <textarea
                                name='otherProduct'
                                rows="10"
                                cols="45"
                                className='typeOther'
                                value={this.state.otherProduct}
                                onChange={this.textValueChanged}
                            />
                        <div className="checkSizeProduct">
                            <input
                                type="checkbox"
                                className='sizeProduct'
                                id='sizeProduct'
                                checked={this.state.bigSizeProduct}
                                onChange={this.sizeProductChanged}
                            />
                            <label htmlFor='sizeProduct' className='sizeProductText'>Груз больших размеров</label>
                        </div>
                    </div>
                </div>
                <div className="item-3">
                    <div className="sistemPay">
                        <h1>Способ оплаты</h1>
                        {payment}
                    </div>
                    <div className="verification">
                        <h1>Подтверждение</h1>
                        <div className="database">
                            <p>Расстояние: {this.state.distance}</p>
                        </div>
                    </div>
                    <div className="userPay">
                        <h1>Итоги к оплате: {this.state.point} с</h1>
                        <button onClick={this.orderClickUser} className='order'>
                            заказать
                        </button>
                        <div className="userVer">
                            <input
                                type="checkbox"
                                checked={this.state.cond}
                                onChange={this.userCondHandle}
                            /><p>Я согласен с условиями обработки персональных данных</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );

        const mapUser = (
            <div>
                <div className="confirm">
                    <h1>От: {this.state.address}</h1>
                    <button onClick={this.mapCloseUser}>Подтвердить</button>
                </div>
                <Map
                    onAPIAvailable={
                        function () {

                        }
                    }
                    center={this.state.center}
                    width={this.state.width}
                    height={this.state.height}
                    zoom={10}
                    onClick={this.fromMapClick.bind(this)}
                >
                    {
                        this.state.coords.map(coords=>(
                            <Marker
                                lat={this.state.coords[0]}
                                lon={this.state.coords[1]}
                                iconContent={this.state.address}
                            />
                        ))
                    }
                </Map>
            </div>
        );

        const transferProductMap = (
            <div>
                <div className="confirm">
                    <h1>Доставка: {this.state.transferAddress}</h1>
                    <button onClick={this.transferMapCloseUser}>Подтвердить</button>
                </div>
                <Map
                    onAPIAvailable={
                        function () {

                        }
                    }
                    center={this.state.center}
                    width={this.state.width}
                    height={this.state.height}
                    zoom={10}
                    onClick={this.toMapClick.bind(this)}
                >
                    {
                        this.state.coords.map(coords=>(
                            <Marker
                                lat={this.state.coordsTransfer[0]}
                                lon={this.state.coordsTransfer[1]}
                            />
                        ))
                    }
                </Map>
            </div>
        );

        return (
            <div>
                {this.state.showMap ? mapUser : mainOrder
                && this.state.transferMapShow ?  transferProductMap : mainOrder
                && this.state.spin ? <Loader/> : mainOrder
                }
            </div>
        );
    }
}

export default FirstPage;