import React from 'react';
import FirstPage from "./components/pages/firstPage";

function App() {
  return (
    <div className="App">
      <FirstPage/>
    </div>
  );
}

export default App;
