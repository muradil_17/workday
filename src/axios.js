import axios from "axios";

const instance = axios.create({
    baseURL: "https://delivery.artwaga.com/api/v1/",
    headers: {
        'Content-Type' : 'application/json',
        'Accept' : 'application/json',
        'Authorization' : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmM2Y1NmM3NGViM2Y4YjA5NDJlNDM3NSIsInJlbWVtYmVyTWUiOnRydWUsImV4cCI6MTYyOTcyNjMxOCwiaWF0IjoxNTk4MTkwMzE4fQ.MsHAryAhIh_MDHtExUvP6AfbxjZw0hWlZEJjKJOWVew'
    },
});

export default instance;
